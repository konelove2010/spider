package com.kone.robot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.security.access.intercept.RunAsManager;

import com.sun.jndi.url.corbaname.corbanameURLContextFactory;

public class Writer implements Runnable {
	public ImageClient imageClient;

	public Writer(ImageClient client) {
		this.imageClient = client;
	}

	public void run() {
		while (true) {
			synchronized (imageClient.allUrls) {
				if (imageClient.allUrls.size() == 0) {
					try {
						if (imageClient.stop) {
							break;
						}
						System.out.println("图片写入线程进入等待["
								+ Thread.currentThread() + "]"
								+ imageClient.allUrls.size());
						imageClient.allUrls.wait();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} else {
					try {
						String url = imageClient.allUrls.pop().getUrl();
						System.out.println("获得�?��url:" + url);
						get(url);
					} catch (Exception e) {
						continue;
					}
				}
			}
		}
		System.out.println("图片写入完成");
	}

	public void get(String url) throws Exception {
		HttpClient client = new HttpClient();
		HttpMethod get = new GetMethod(url);
		get.setRequestHeader(ImageClient.USER_AGENT_H, ImageClient.USER_AGENT);
		get.setRequestHeader(ImageClient.REFERER_H, url);
		int status = client.executeMethod(get);
		System.out.println(status);
		if (status == HttpStatus.SC_OK) {
			InputStream in = get.getResponseBodyAsStream();
			Document doc = Jsoup.parse(in, "UTF-8", "");
			Elements imgs = doc.select("img");
			System.out.println(imgs.size());
			for (Element img : imgs) {
				System.out.println(img.absUrl("src"));
				try {
					new Thread(new Save(img.absUrl("src"), url)).start();
				} catch (Exception e) {
					continue;
				}
			}
		}
	}

	class Save implements Runnable {
		private String img;
		private String ur;

		public Save(String img, String ur) {
			super();
			this.img = img;
			this.ur = ur;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			File file = new File(ImageClient.path);
			if (!file.exists())
				file.mkdirs();
			String name = UUID.randomUUID().toString();
			File f = new File(file.getAbsolutePath() + File.separator + name
					+ "." + ImageClient.EXT);
			URLConnection conn;
			try {
				if (StringUtils.isBlank(img)) {
					Thread.yield();
				}
				URL url = new URL(img);
				conn = url.openConnection();
				System.out.println("[写入图片][线程" + Thread.currentThread() + "]:"
						+ img);
				conn.setRequestProperty(ImageClient.REFERER_H, ur);
				ImageIO.write(ImageIO.read(conn.getInputStream()),
						ImageClient.EXT, new FileOutputStream(f));
			} catch (Exception e) {
				Thread.yield();
			}
		}
	}

}
